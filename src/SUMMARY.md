# Summary

[Overview](overview.md)

- [General guidelines](general-guidelines.md)
- [Base library](base-library.md)
- [Patterns](patterns.md)
    - [Centralized Globals](patterns/globals.md)
    - [Including other files](patterns/including-other-files.md)
    - [Custom job images](patterns/custom-job-images.md)
    - [Job labels](patterns/job-labels.md)
    - [CICD State](patterns/cicd-state.md) 
    - [Constants](patterns/constants.md)
    - [Variable setenv/getenv](patterns/setenv-getenv.md)
    - [Version numbering](patterns/version-numbering.md)
    - [Phase skipping](patterns/phase-skipping.md)
    - [Pipeline conditions](patterns/pipeline-conditions.md)
    - [Static docs sites](patterns/static-docs-site.md)
- [Contributing](contributing.md)