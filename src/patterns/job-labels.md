# Job labels

_STATUS: Draft: Some structure and useful information - test and refine before use_


## Opportunity: Include the same CICD element multiple times

Example:  some teams need to deploy to multiple environments or clusters, with
slightly different settings for each, but a similar script. Inputs make it easy
to keep the configuration DRY while keeping the parameters local.

A client needed to deploy to a custom platform across multiple
environments and regions. To avoid repetition, we include the same element more
than once.

In `deploy-phase.gitlab-ci.yml`:

```yaml
  - local: deploy-job.gitlab-ci.yml
    inputs:
      region: west
      env: stage
  - local: deploy-job.gitlab-ci.yml
    inputs:
      region: east
      env: stage
  - local: deploy-job.gitlab-ci.yml
    inputs:
      region: west
      env: prod
  # ...etc...
```

## Problem: Job name collision

Job names are global to the entire pipeline, across all `include:` elements,
including base jobs. In the example above, if the `deploy-job` element
included a job such as `deploy`, YAML interpolation would overwrite
the job name each time, and only one job would appear in the pipeline. The
impact confuses some developers.

## Solution: Reference unique inputs in job names

In `deploy-job.gitlab-ci.yml`:

```yaml
spec:
  inputs:
    region:
    env:
---
deploy-$[[inputs.env]]-$[[inputs.region]]:
  # ... other aspects of job definition ...
  script:
    # ... other parts of script ...
    - mvn deploy -Denv=$[[inputs.env]] -Dregion=$[[inputs.region]]
```

The jobs appear in the pipeline UI and API responses with unique names such as `deploy-dev-west`.

## Hot tip: Use explicit job labels

For many job elements, multiple use cases aren't apparent when the job is
created. But others might come up with reasons to use a job more than once.
Consider using an explicit job label in **every** job element in a library.

In `some-test-job.gitlab-ci.yml`:

```yaml
spec:
  inputs:
    job-label:
      default: job
      description: >-
        Set to differentiate jobs if using this element more than once in a pipeline.
    # ... other inputs ...
---
some-test-$[[inputs.job-label]]:
    # ... job definition ...
```

The base library usess the pattern extensively ([example](https://gitlab.com/procicd/lib/-/blob/base/python/unittest-job.gitlab-ci.yml)).


        
