# Centralized Globals

_STATUS Sketchy: Author brainstormed ideas and recorded them - read with caution_

We use the term "globals" to refer to YAML objects at the "root" level (leftmost column) of an element's YAML file that is NOT a job. Examples of "global" objects include:

- `stages:`
- `default:`
- `workflow:`
- global `variables:`
- base jobs (starting with "`.`")

Globals apply to all the jobs in a pipeline, including other elements added with `include:`. Furthermore, few of them are extensible (`stages:` for example), so different instances of the object in different elements might unintentionally override each other.

## The all-global element

The base library includes an element at `util/all-global` intended to hold global values for use in all pipelines.

### Recommendations

- Include `util/all-global` in every `-pipe` element

### Usage

See the `python/pypi-library-pipe` element for an example of usage:

```yaml
include:
  - local: 'util/all-global.gitlab-ci.yml'
```

### Contents

The `util/all-global` element contains functionality to support patterns described in this guide throughout the library:

- [Constants pattern](patters/constants.md)
- [SetEnv and GetEnv pattern](patterns/setenv-getenv.md)
- [Custom job images pattern](patterns/custom-job-images.md)
- [Pipeline conditions pattern](patterns/pipeline-conditions.md)

It also includes:

- Definitions of `stages` to be used across all pipeline definitions - a longer and more granular, but still generalized, list of stages than provided by default in GitLab, to allow for more sophisticated pipelines. Ohter elements in the base library reference the stages defined here and expect them to occur in that order.
- A handy `display` function to highlight important output in job logs
- Default job image setting for `alpine`

## The root-level default antipattern

_TODO_