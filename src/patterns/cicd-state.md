# CICD state

_STATUS Sketchy: Author brainstormed ideas and recorded them - read with caution_

The library supports a mechanism for saving the state of CICD pipelines in the middle for later reuse within pipelines of the same ref. For example, a project's built package can be saved for deployment in a later pipeline.

To save and load state, include `cicd-state-job.gitlab-ci.yml`.

The CICD state mechanism saves all the artifacts available in the pipeline at the point of saving, placing them into the GitLab generic package registry indexed by Git ref. The loading job then pulls the latest entry from the registry.
