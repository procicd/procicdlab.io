# Constants

_STATUS Sketchy: Author brainstormed ideas and recorded them - read with caution_


CICD Variables are, in fact, variable: they can differ from one pipeline run to the next. Also, variable precedence means that values defined in CICD YAML can be overwritten at the group/project level in GitLab for a specific pipeline run, sometimes overriding important logic. 

Sometimes we use CICD variables to represent constants, that is:

- The value stays the same during a pipeline
- The value is the same for each pipeline run
- The value is the same for pipelines run in multiple projects accesssing a library

In these cases, sometimes we use a group-level variable, creating a tight coupling between database data (the variable setting) and configuration (in the YAML). Such a situation can confuse future operators, who have to search to figure out where the variable it set. An alternative is to set variables in the CICD library itself, which results in a situation akin to hard-coding values in code. Instead of both, we recommend using a single file within the CICD library to hold all the constants, and the bas library provides a mechanism to do so.

The Constants pattern says:

## _Manage site-wide constants (not true variables, not secrets) in a single YAML file_

---

## The constants.yml file

The base library contains everything necessary to manage constants as described by the pattern:

- A placeholder `constants.yml` file - put your values here
- A function `constant` to read from the YAML file in a Bash script at runtime using YQ
- An `include:` object using `util/save-package-file-job` to save the `constants.yml` file to a generic package in the library (part of the library's own `.gitlab-ci.yml` element)
- An `include:` object using `util/load-package-file-job` to load the `constants.yml` file at the beginning of every pipeline run to make the values available to scripts within the pipeline (part of `all-global`)

### Shortcomings

- Constants loaded this way can only be used within scripts - they are not available at the time of YAML interpretation so can't be used in environment names, etc.

_TODO_ Needs more examples and clarity!
