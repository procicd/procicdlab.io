# Including other files

_STATUS Sketchy: Author brainstormed ideas and recorded them - read with caution_

Often it's necessary to include files other than CICD YAML (such as scripts and configuration files) for use in a job. The `include:` mechanism only supports CICD YAML. Here are other options, which might apply in different situations:

## Script text in YAML

Some shell scripts are too long or complicated to go into a YAML array nicely, but still belong in the CICD element. Try including the script text in the element itself at the end using a single [YAML multiline string](https://yaml-multiline.info/) in a [YAML anchor](https://docs.gitlab.com/ee/ci/yaml/yaml_optimization.html#anchors)

```yaml
.my-script: &my-script |
  function my_function() {
    echo "Hello $1!"
  }

my-job:
  script:
    - *my-script
    - my_function 'World'
```

In a long CICD element, sometimes it's easier to keep the big script block at the end of the element, after all the YAML. But an anchor definition has to appear before its reference. Consider putting a base job at the end of the file (only works if the multiline script is the only entry in `before_script`).

```yaml
my-job:
  extends: [.my-script]
  script:
    - my_function 'World'

# ... other configuration ...

.my-script:
  before_script:
    - |
        function my_function() {
            echo "Hello $1!"
        }

        # ... other script ...
```

As an example, the Salesforce.com team provided a set of Bash functions in a multiline YAML anchor for the SFDX integration project ([example](https://gitlab.com/sfdx/sfdx-cicd-template/-/blob/1b85ae561649a9424c183d085fdb87e0e4cc0d9a/Salesforce.gitlab-ci.yml#L289)).

## Other techniques

- Put the whole script in a group CICD variable - not recommended
- Prebuild a [custom job image](custom-job-images.md)
- Publish to the generic package registry
- Use `needs:project:`
