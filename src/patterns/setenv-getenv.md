# SetEnv and GetEnv

_STATUS Sketchy: Author brainstormed ideas and recorded them - read with caution_

Instead of using `artifacts:reports:dotenv:`, use the `setenv`/`getenv` pattern defined in the `all-global` element. It relies on job artifacts, which are more tightly constrained and can be saved.

The files we save with variable information receive a `.html` filename extension so they can be easily viewed in the artifact browser in GitLab (otherwise a .sh or .txt extension would make more sense).

_TODO_