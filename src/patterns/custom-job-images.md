# Custom job images

_STATUS Sketchy: Author brainstormed ideas and recorded them - read with caution_

## Why custom job images

...

## Suggested guidelines

1. Keep the source code (Dockerfile) for custom job images in the CICD library project to manage all the configuration related to one operation in the same place.
1. Publish custom job images to the GitLab container registry -- even if the company has a policy of storing all container images in an external platform, it's MUCH simpler to keep custom job images -- which are both created and consumed by GitLab -- in GitLab itself.
1. _Dockerfile..._
1. _job image version..._
1. _how to build in an MR..._

## Proposed conventions

- Directory structure: `images/` at the root of the library, with a subfolder for each image
- `Dockerfile` and `README.md` (minimum) in each image folder, along with other files required
- `.gitlab-ci.yml` fiel within each image directory with instructions for building that image -- typically a reference to `docker/build-job.gitlab-ci.yml` with `inputs:`.
- In the main `.gitlab-ci.yml` for the library, an `include:` entry for each image to be built.

See the [MDBook](https://gitlab.com/procicd/lib/-/tree/base/images/mdbook) image in the base library for a simple example, or [Maven](https://gitlab.com/procicd/lib/-/tree/base/images/maven) for one that includes other files.


## Using a custom job image

1. _extend custom-job-image-registry base_
1. _Understand the JOB IMAGE variables_

## Author notes

- _Better approach to versioning of custom images_
- _Workarounds for [no variables in MR pipelines](https://gitlab.com/gitlab-org/gitlab/-/issues/452550)?_
- _Here explain the logic in root CI config and in the build job_
