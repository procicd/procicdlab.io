# Pipeline conditions

_STATUS Sketchy: Author brainstormed ideas and recorded them - read with caution_

The different pipeline types and conditions under which pipelines run can present challenges. Without attention, a project can end up with multiple pipelines running on the same push, pipelines triggering other pipelines in a loop, or simple confusion.

## Guidelines and detailed pipeline types

In the base library, we define a more detailed set of pipeline types, then use a variable and the pipeline name in the UI to annotate them. The guidelines are:

- Never run tag pipelines; insead add release jobs to the same pipeline that creates the tag
- Only run branch pipelines on the default branch or specific branches (i.e. for hotfixes)
- Use merge request pipelines exclusively within merge request branches
- Try to avoid pipeline runs when the only activity is creating a branch with no changes
- If someone makes and pushes a branch outside of a merge request, do nothing
- Use global pipeline rules to control and annotate different types of operations


## Referencing conditions

`$CONDITION` can be referenced in job-level rules. For example,

```yaml
if: $CONDITION =~ /pipelinetest/
```

or

```yaml
if: $CONDITION =~ /main|hotfix/
```


## Defining conditions

The `util/all-global` element in the base library handles pipeline condition logic, sets the `$CONDITION` variable, and generates the correct title for the GitLab UI.

