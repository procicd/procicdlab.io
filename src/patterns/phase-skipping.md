# Phase skipping

_STATUS Rough: Undergone lightweight review and/or testing - reference it to discern broad themes or extract examples_

When developing and testing CICD elements, it's often necessary to run just certain jobs from a pipeline, and to turn jobs off and on, to get through testing quickly. We recommend two techniques, both of which work at the "phase" level (see [yaml element types](/base-library.md#elements-yaml-files)) so across a set of related/interdependent jobs. Combine both approaches to allow phase skipping with either an input (in the YAML itself) or a variable (which could be temporarily set in the test project or at runtime).

It's set up in the `-pipe` element. As an example, to allow skipping of a `test` phase,  add `skip-test` to `inputs:` and support a variable called `$CICD_SKIP_TEST`.

Example of phase skipping:

```yaml
  - local: mytype/mytype-test-phase.gitlab-ci.yml
    rules:
      - if: "'$[[inputs.skip-test]]'=='' && $CICD_SKIP_TEST==''"
```
