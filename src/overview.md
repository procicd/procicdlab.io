# About CICD library projects

GitLab CICD provides a powerful general-purpose automation platform for running a wide variety of workloads, including Continuous Integration, Continuous Delivery, ModelOps, and GitOps. The platform also includes a powerful collection of templates and components to configure CICD rapidly for common use cases.

Organizations sometimes require specific, highly-customized configurations to meet sophisticated business and technical requirements. In such cases, a CICD library project provides a useful mechanism to develop, manage, and maintain pipeline configuration, job images, and scripts.

Similar to the [CICD Catalog](https://gitlab.com/explore/catalog), but different. A CICD library project leverages key technology within the Components initiative at GitLab, and in some cases even uses components from the catalog, but is managed within an organization for use in its projects.

_**Always a work in progress. Always a [draft](https://handbook.gitlab.com/handbook/values/#everything-is-in-draft).**_


## Framework

The ProCICD initiative includes a rough "framework" that can be used as a starting point for a CICD library project. Parts of the framework include:

- [ProCICD guide](https://procicd.gitlab.io) - This document
- [ProCICD base library](https://gitlab.com/procicd/lib) - Starting point for custom CICD libraries
- [Token tool](https://gitlab.com/procicd/token-tool) - Script to encode a GitLab deploy token
- [test projects](https://gitlab.com/procicd/test) - A few projects designed to exercise specific capabilities of the framework

## Terminology

ProCICD uses specific terminology, some of which differs slightly from GitLab convention.

- **CICD** - Continuous Integration and Continuous Delivery. Sometimes written "CI/CD" or "CI-CD" but we prefer to leave out the punctuation to emphasize that GitLab pipelines perform all operations under one automation system (and it saves a keystroke).
- **Element** - A YAML file containing GitLab CICD job definitions and other pipeline configuration, referenced via `include:project:` and `include:file:`. We chose the term "element" to differentiate from "templates" and "components" (although the meaning overlaps).
- **Object** - Any key-value pair in YAML, including the entire value.
- **Base job** - CICD job whose names begin with a dot ("."), referred to as  "hidden job" in the official GitLab documentation, that act in a similar way to base classes in object-oriented programming (the word "hidden" implies invisible activity so can confuse us).
- **Pipeline rules** - Rules that determine whether an entire pipeline is run, and sets variables based on conditions. Pipeline rules use `workflow:rules:` objects in YAML but we find "pipeline rules" a more accurate term than "workflow rules".
- **Delivery** - Includes "deployment" and/or "distribution" (see below); conveniently, all 3 words start with the letter "D".
- **Deploy** - Push an application to a server/hosting/cluster environment where it will operate.
- **Distribute** - Push an application or library to a storage location such as a container or package registry.
- **Release** - Generally refers to version numbering and Git tagging operations, distinct from actual deployment and distribution

GitLab CICD documentation and practice uses two other terms that lie outside the scope of custom CICD library contents, which is why we use the term "element" to refer to YAML files in the library itself. Still, understanding helps.

- **Template** - GitLab-produced YAML that comes with the GitLab system, viewable on [GitLab.com](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib/gitlab/ci/templates), referenced via `include:template:`. The template collection will soon be deprecated in favour of components (below).
- **Component** - Newer replacement for "templates" that may be developed and contributed by the GitLab community, viewable in the [components catalog](https://gitlab.com/explore/catalog), referenced via `include:component:`.

The framework suggests a `gitlab` directory for "wrapping" templates and components if required.

## Document and element statuses

We've tried to standardize the "status" at the tops of pages (in this guide) and elements (in the base library). The statuses are:

- Sketchy: Author brainstormed ideas and recorded them - read with caution
- Rough: Undergone lightweight review and/or testing - reference it to discern broad themes or extract examples
- Draft: Some structure and useful information - test and refine before use
- Stable: Someone other than the author has read or used this - proceed with confidence

<br/>
<br/>
---

By [Francis Potter](https://fpotter.gitlab.io), based on over 5 years of experience as a GitLab CICD professional and work on [personal projects](https://gitlab.com/wizlib/).

Freely distributed. None of the content herein represents official GitLab marketing or documentation. Documents, code, and configuration might change at any time without notice. Intended for customization and use by experienced GitLab practitioners.

[Guide](https://procicd.gitlab.io) published using MDBook and the ProCICD base library itself.
