# Patterns

The ProCICD framework uses several patterns, akin to [software design patterns](https://en.wikipedia.org/wiki/Software_design_pattern), that reflect useful approaches to addressing common CICD requirements. In many cases, the elements in the base library can be copied and used as provided to implement the pattern; in other cases custom implementations are required.

Documented patterns include:

- [Centralized Globals](patterns/globals.md)
- [Including other files](patterns/including-other-files.md)
- [Custom job images](patterns/custom-job-images.md)
- [Job labels](patterns/job-labels.md)
- [CICD State](patterns/cicd-state.md) 
- [Constants](patterns/constants.md)
- [Variable setenv/getenv](patterns/setenv-getenv.md)
- [Version numbering](patterns/version-numbering.md)
- [Phase skipping](patterns/phase-skipping.md)
- [Pipeline conditions](patterns/pipeline-conditions.md)
- [Static docs sites](patterns/static-docs-site.md)


_TODO: Add pattern for variables as error messages, outputting commands before running, etc_