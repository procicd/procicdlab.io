# Contributing to a CICD library

_STATUS Rough: Undergone lightweight review and/or testing - reference it to discern broad themes or extract examples_

## General guidelines

- Treat the pipeline like a series of loosely coupled microservices, with each job performing only one function.
- If referencing one of the GitLab "templates", wrap it to provide a place to keep standard overrides and customizations - the wrappers live in the `gitlab/` directory.
- Remember that arrays cannot be extended in an `extends` or override case - especially tricky with `before_script` - though the `!reference` form can be useful.
- See YAML comments for more notes; the `maven` directory contains the most up-to-date examples of best practices.

## Contributing to the library

The library includes a mechanism for testing changes to the library itself

1. Pick a test project for testing out the pipeline changes (preferably with a small blast radius if things go wrong)
1. Create a branch and MR in this library project to work on the changes
1. Create a branch and MR in the test project you're using to work on the changes 1. In the test project, add a `ref:` to the `include:` pointing to the working branch of the library
1. Add a `cicd-pipeline-test` label to the MR in the test project
1. When developing, test for the `$CONDITION=~/pipelinetest/` (see `maven-base.gitlab-ci.yml` for an example) as necessary to detect test runs (for example, to override branch name tests)
1. When complete, merge the MR in the library
1. Remove the `ref:` from the MR in the test project and merge it if needed
1. Generate a new numbered release of the library (using the `VERSION_INCREMENT` variable - to be implemented)

## Upstreaming to the ProCICD base library

_TODO_
